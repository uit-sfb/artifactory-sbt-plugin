name := "inner"

resolvers ++= {
  Seq(
    Some(
      "Artifactory" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"),
    if (version.value.endsWith("-SNAPSHOT"))
      Some(
        "Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local/")
    else
      None
  ).flatten
}
