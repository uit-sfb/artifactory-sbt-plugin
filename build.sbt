name := "artifactory-sbt-plugin"
organization := "no.uit.sfb"

sbtPlugin := true

enablePlugins(GitVersioning)
useJGit
git.gitlabCiOverride := true


publishTo := {
  if (version.value.endsWith("-SNAPSHOT"))
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-dev-local;build.timestamp=" + new java.util.Date().getTime)
  else
    Some("Artifactory Realm" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local")
}

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")