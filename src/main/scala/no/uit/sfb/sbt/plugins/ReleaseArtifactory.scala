package no.uit.sfb.sbt.plugins

import sbt.{AutoPlugin, Credentials, DirectCredentials, config}

import java.nio.file.{Path, Paths}

import sbt._

import sys.process._

object ReleaseArtifactory extends AutoPlugin {
  case class DownloadConfig(
      ancestorCommitId: String = "",
      path: Path = Paths.get("/"),
      outPath: Option[Path] = None
  )

  case class FromAncestorConfig(
      commitId: String = "",
      follow: Boolean = false
  )

  case class DeployConfig(forcePull: Boolean = false)

  import autoImport._

  override lazy val projectSettings = Seq(
    Artifactory / artifCreds := {
      Credentials
        .loadCredentials(sbt.Path.userHome / ".sbt" / ".credentials")
        .toOption
    },
    Artifactory / artifUser := {
      (Artifactory / artifCreds).value.get.userName
    },
    Artifactory / artifPassword := {
      (Artifactory / artifCreds).value.get.passwd
    },
    Artifactory / artifHost := {
      (Artifactory / artifCreds).value.get.host
    },
    Artifactory / artifArtifacts := {
      val paths = (Artifactory / artifBuildArtifacts).value
      val _host = (Artifactory / artifHost).value
      val _location = (Artifactory / artifLocation).value
      val _buildVersion = (Artifactory / artifProjectVersion).value
      (paths map { path =>
        path -> s"https://${_host}/artifactory/generic-local/${_location}/${_buildVersion}/${path.getFileName}"
      }).toMap
    },
    Artifactory / publish := {
      val artifs = (Artifactory / artifArtifacts).value
      val _user = (Artifactory / artifUser).value
      val _password = (Artifactory / artifPassword).value
      artifs foreach {
        case (path, url) =>
          println(s"Publishing $path to $url...")
          val ret =
            s"""curl -u ${_user}:${_password} --silent --output /dev/null --write-out "%{http_code}" -T $path $url""".!!.replace(
              "\"",
              "").trim()
          if (ret != "201") {
            throw new Exception(s"Upload failed with code $ret")
          }
      }
    }
  )

  object autoImport {
    lazy val Artifactory = config("artifactory") describedAs "Artifactory release plugin"

    /**
      * Public
      */
    lazy val artifUser =
      sbt.taskKey[String]("Artifactory user")
    lazy val artifPassword =
      sbt.taskKey[String]("Artifactory password")
    lazy val artifHost =
      sbt.taskKey[String]("Artifactory host")
    lazy val publish =
      sbt.taskKey[Unit]("Publish to artifactory")

    /**
      * Internal
      */
    lazy val artifBuildArtifacts =
      sbt.taskKey[Seq[Path]]("Build artifacts and return paths")
    lazy val artifProjectVersion =
      sbt.settingKey[String]("Project version")
    lazy val artifLocation =
      sbt.settingKey[String]("Where in generic-local to publish the artifact")
  }

  lazy val artifArtifacts =
    sbt.taskKey[Map[Path, String]]("Map(path -> artifact url)")
  lazy val artifCreds =
    sbt.settingKey[Option[DirectCredentials]]("Artifactory credentials")
}
