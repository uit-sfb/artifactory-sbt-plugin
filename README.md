# Artifactory SBT plugin

An SBT plugin to publish artifacts to artifactory

## Setup

- Add to plugin to `project/plugins.sbt`: `addSbtPlugin("no.uit.sfb" % "artifactory-sbt-plugin" % "0.1.0")`
- Create `project/build.sbt` containing: `resolvers += "Artifactory-dev" at "https://artifactory.metapipe.uit.no/artifactory/sbt-release-local/"`
- setup plugin in `build.sbt`
    - enable plugin: `enablePlugins(ReleaseArtifactory)`
    - define plugin settings:
```
Artifactory / artifProjectVersion := version.value
Artifactory / artifBuildArtifacts := <build artifacts and return paths> // For example: Seq((Universal / packageBin).value.toPath)
Artifactory / artifLocation := <where to upload artifacts (under generic-local)> //For example: s"${organization.value}/${name.value}"
```

## Usage

`sbt artifactory:publish`

